/*
*  Copyright (c) 2016-2017 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2017/05/11
*
*/

using UnrealBuildTool;

public class QRCodeUE4Demo : ModuleRules
{
	public QRCodeUE4Demo(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivatePCHHeaderFile = "QRCodeUE4Demo.h";

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
