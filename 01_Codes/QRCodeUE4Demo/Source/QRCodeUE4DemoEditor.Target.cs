/*
*  Copyright (c) 2016-2017 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2017/05/11
*
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class QRCodeUE4DemoEditorTarget : TargetRules
{
	public QRCodeUE4DemoEditorTarget(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Editor;

        ExtraModuleNames.AddRange(new string[] { "QRCodeUE4Demo" });
    }
}
