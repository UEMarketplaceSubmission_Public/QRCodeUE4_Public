/*
*  Copyright (c) 2016-2017 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2017/05/11
*
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class QRCodeUE4DemoTarget : TargetRules
{
	public QRCodeUE4DemoTarget(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Game;

        ExtraModuleNames.AddRange(new string[] { "QRCodeUE4Demo" });
    }
}
