# 1. QRCodeUE4

**"QRCodeUE4"(Friendly name is "用于UE4的二维码生成器") is a UE4 C++ Plugin.**     
This plugin is used to generate QRCode (Two Dimensional Code，二维码) in real time in UE4 Game.
Only need to use the Blueprint Functions of this Plugin, you can achieve real-time generation of QRCode.
You can use our plugin to convert any string to QRCode (2D code) information.
Of course, you can also convert QRCode (2D code) information to SVG string, or get the Boolean value for each bit of the QRCode (2D code).

## 1.1 Get QRCodeUE4

你可以从虚幻4商城中购买“QRCodeUE4”插件。在搜索栏中搜索“QRCode”可以找到该插件。

## 1.2 Enable QRCodeUE4

安装插件后，请按照下图所示，在你的UE4工程项目中启用该插件。

![iLocalPlayerUE4Demo_01 01_F9_01](README.md.resources/01_Images/01_EnablePlugin/001_EnablePlugin.PNG)

## 1.3 How to use QRCodeUE4

### 1.3.1 QRCodeDataToQRCodeSvgString

打开示例项目"[QRCodeUE4Demo](https://gitlab.com/UE4MarketplaceSubmission_Public/QRCodeUE4_Public/tree/master/01_Codes/QRCodeUE4Demo "QRCodeUE4Demo")"，进入`“MAP_QRCodeUE4_Overview_01”`地图(位于“QRCodeUE4”这个插件的Content文件夹中，也许你需要启用“Show Plugin Content”才能看到该插件的Content文件夹)。点击“Play”按钮，你将会看到将字符串转换为QRCode图片的示例效果。

在这个示例中，使用了“EncodeQRCode”蓝图函数结点，将字符串信息转换为“QRCodeData”数据；

![001_EncodeQRCode](README.md.resources/01_Images/02_BlueprintNodes/001_EncodeQRCode.PNG)

再使用“QRCodeDataToQRCodeSvgString”蓝图函数结点，将“QRCodeData”数据转换为SVG格式的字符串信息。

![002_QRCodeDataToQRCodeSvgString](README.md.resources/01_Images/02_BlueprintNodes/002_QRCodeDataToQRCodeSvgString.PNG)

得到Svg格式的字符串信息后，使用"Web Browser" UMG Widget 渲染该SVG格式的字符串信息表示的二维码图片效果。

![001_RenderSVGString](README.md.resources/01_Images/03_RenderSVGString/001_RenderSVGString.PNG)

最终的效果如下图所示：

![002_RenderSVGString_Result](README.md.resources/01_Images/03_RenderSVGString/002_RenderSVGString_Result.PNG)

### 1.3.2 GetBitOfQRCodeData

打开示例项目"[QRCodeUE4Demo](https://gitlab.com/UE4MarketplaceSubmission_Public/QRCodeUE4_Public/tree/master/01_Codes/QRCodeUE4Demo "QRCodeUE4Demo")"，进入`“MAP_QRCodeUE4_Overview_02”`地图(位于“QRCodeUE4”这个插件的Content目录中，也许你需要启用“Show Plugin Content”才能看到该插件的Content文件夹)。点击“Play”按钮，你将会看到将字符串转换为QRCode（二维码）三维模型的效果。

在这个示例中，使用了“EncodeQRCode”蓝图函数结点，将字符串信息转换为“QRCodeData”数据；

![001_EncodeQRCode](README.md.resources/01_Images/04_GetBitOfQRCodeData/001_EncodeQRCode.PNG)

再从“QRCodeData”中获取该二维码的尺寸；

![002_QRCodeData_Size](README.md.resources/01_Images/04_GetBitOfQRCodeData/002_QRCodeData_Size.PNG)

最后使用“GetBitOfQRCodeData”蓝图函数结点，获取“QRCodeData”中的每一位的布尔值；

![003_GetBitOfQRCodeData](README.md.resources/01_Images/04_GetBitOfQRCodeData/003_GetBitOfQRCodeData.PNG)

最终的效果如下图所示：

![003_GetBitOfQRCodeData](README.md.resources/01_Images/05_3DQRCodeModels/001_3DQRCodeModels_Result.PNG)