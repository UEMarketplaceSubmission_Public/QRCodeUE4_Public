# 1. QRCodeUE4

**"QRCodeUE4"(Friendly name is "QRCode Generator For UE4") is a UE4 C++ Plugin.**     
This plugin is used to generate QRCode (Two Dimensional Code) in real time in UE4 Game.
Only need to use the Blueprint Functions of this Plugin, you can achieve real-time generation of QRCode.
You can use our plugin to convert any string to QRCode (2D code) information.
Of course, you can also convert QRCode (2D code) information to SVG string, or get the Boolean value for each bit of the QRCode (2D code).

## 1.1 Get QRCodeUE4

You can buy "QRCodeUE4" plugin from UE4 Marketplace. Search for keyword "QRCode" in the search bar to find the plugin.

## 1.2 Enable QRCodeUE4

After installing the “QRCodeUE4” Plugin, please enable the plug-in in your UE4 project as shown below.

![001_EnablePlugin](README.md.resources/01_Images/01_EnablePlugin/001_EnablePlugin.PNG)

## 1.3 How to use QRCodeUE4

### 1.3.1 QRCodeDataToQRCodeSvgString

Open the sample project "[QRCodeUE4Demo](https://gitlab.com/UE4MarketplaceSubmission_Public/QRCodeUE4_Public/tree/master/01_Codes/QRCodeUE4Demo "QRCodeUE4Demo")", and open the `"MAP_QRCodeUE4_Overview_01"` map (in the content folder of "QRCodeUE4" plugin, maybe you need to enable "show plugin content" to see the plugin's content folder). Click the "Play" button. You will see a sample effect of converting a string to a QRCode image.

In this sample, the "EncodeQRCode" blueprint function node is used to convert the string information to "QRCodeData" data;

![001_EncodeQRCode](README.md.resources/01_Images/02_BlueprintNodes/001_EncodeQRCode.PNG)

Then use "QRCodeDataToQRCodeSvgString" blueprint function node, convert the "QRCodeData" data to SVG format string information.

![002_QRCodeDataToQRCodeSvgString](README.md.resources/01_Images/02_BlueprintNodes/002_QRCodeDataToQRCodeSvgString.PNG)

After getting the SVG-formatted string information, use the "Web Browser" UMG Widget to render the 2D QRCode image effect of the SVG-formatted string information.

![001_RenderSVGString](README.md.resources/01_Images/03_RenderSVGString/001_RenderSVGString.PNG)

The final result is shown below:

![002_RenderSVGString_Result](README.md.resources/01_Images/03_RenderSVGString/002_RenderSVGString_Result.PNG)

### 1.3.2 GetBitOfQRCodeData

Open the sample project "[QRCodeUE4Demo](https://gitlab.com/UE4MarketplaceSubmission_Public/QRCodeUE4_Public/tree/master/01_Codes/QRCodeUE4Demo "QRCodeUE4Demo")", and open the `“MAP_QRCodeUE4_Overview_02”` map (in the content folder of "QRCodeUE4" plugin, maybe you need to enable "show plugin content" to see the plugin's content folder). Click the "Play" button. You will see the QRCode 3D model effects that are converted from string.

In this sample, the "EncodeQRCode" blueprint function node is used to convert the string information to "QRCodeData" data;

![001_EncodeQRCode](README.md.resources/01_Images/04_GetBitOfQRCodeData/001_EncodeQRCode.PNG)

And then from "QRCodeData" to obtain the size of QRCode code;

![002_QRCodeData_Size](README.md.resources/01_Images/04_GetBitOfQRCodeData/002_QRCodeData_Size.PNG)

Finally, use "GetBitOfQRCodeData" blueprint function node to get every bit boolean value of the "QRCodeData" data.

![003_GetBitOfQRCodeData](README.md.resources/01_Images/04_GetBitOfQRCodeData/003_GetBitOfQRCodeData.PNG)

The final result is shown below:

![003_GetBitOfQRCodeData](README.md.resources/01_Images/05_3DQRCodeModels/001_3DQRCodeModels_Result.PNG)